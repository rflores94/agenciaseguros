package agenciaSeguros.gui;

import java.awt.EventQueue;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Clase principal
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class Principal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected File file = new File("SinTitulo.obj");
	private static Principal frame;
	private Ayuda ayuda = new Ayuda();
	private static final String AGENCIA_SEGUROS = "Agencia de seguros";

	/**
	 * Launch the application.
	 * @param args Args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				General.salir(frame);
			}
		});
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(getNombreAplicacion() + ": " + file.getName());
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);

		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mntmNuevo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				General.crearNuevo(frame);
			}
		});
		mnArchivo.add(mntmNuevo);

		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mntmAbrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
		mntmAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				General.abrirFichero(frame);
			}
		});
		mnArchivo.add(mntmAbrir);

		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mntmGuardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
		mntmGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				General.guardar(frame);
			}
		});
		mnArchivo.add(mntmGuardar);

		JSeparator separator = new JSeparator();
		mnArchivo.add(separator);

		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como");
		mntmGuardarComo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				General.guardarComo(frame);
			}
		});
		mnArchivo.add(mntmGuardarComo);

		JSeparator separator_1 = new JSeparator();
		mnArchivo.add(separator_1);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				General.salir(frame);
			}
		});
		mnArchivo.add(mntmSalir);

		JMenu mnPolizas = new JMenu("Polizas");
		menuBar.add(mnPolizas);

		JMenuItem mntmAadir = new JMenuItem("A\u00F1adir");
		mntmAadir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mntmAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CrearPoliza crearPoliza = new CrearPoliza();
				crearPoliza.setVisible(true);
			}
		});
		mnPolizas.add(mntmAadir);

		JMenuItem mntmEliminar = new JMenuItem("Eliminar");
		mntmEliminar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mntmEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!comprobarPolizasVacias()) {
					EliminarPoliza eliminarPoliza = new EliminarPoliza();
					eliminarPoliza.setVisible(true);
				}
			}
		});
		mnPolizas.add(mntmEliminar);

		JMenuItem mntmMostrar = new JMenuItem("Mostrar");
		mntmMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!comprobarPolizasVacias()) {
					MostrarPolizas mostrarPolizas = new MostrarPolizas();
					mostrarPolizas.setVisible(true);
				}
			}
		});
		mntmMostrar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mnPolizas.add(mntmMostrar);

		JSeparator separator_2 = new JSeparator();
		mnPolizas.add(separator_2);

		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mntmBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!comprobarPolizasVacias()) {
					BuscarPoliza buscar = new BuscarPoliza();
					buscar.setVisible(true);
				}
			}
		});
		mnPolizas.add(mntmBuscar);

		JMenu mnSeguros = new JMenu("Seguros");
		menuBar.add(mnSeguros);

		JMenuItem mntmAadir_1 = new JMenuItem("A\u00F1adir");
		mntmAadir_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmAadir_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!comprobarPolizasVacias()) {
					CrearSeguro crear = new CrearSeguro();
					crear.setVisible(true);
				}
			}
		});
		mnSeguros.add(mntmAadir_1);

		JMenuItem mntmEliminar_1 = new JMenuItem("Eliminar");
		mntmEliminar_1
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmEliminar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!comprobarPolizasVacias()) {
					EliminarSeguro eliminarSeguro = new EliminarSeguro();
					eliminarSeguro.setVisible(true);
				}
			}
		});
		mnSeguros.add(mntmEliminar_1);

		JSeparator separator_3 = new JSeparator();
		mnSeguros.add(separator_3);

		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!comprobarPolizasVacias()) {
					EditarSeguro editarSeguro = new EditarSeguro();
					editarSeguro.setVisible(true);
				}
			}
		});
		mntmEditar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnSeguros.add(mntmEditar);

		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);

		JMenuItem mntmMostrarAyuda = new JMenuItem("Mostrar Ayuda");
		mntmMostrarAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ayuda.setVisible(true);
			}
		});
		mnAyuda.add(mntmMostrarAyuda);

		JMenuItem mntmSobreAgenciaSeguros = new JMenuItem("Sobre Agencia Seguros");
		mntmSobreAgenciaSeguros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AcercaDe acercaDe = new AcercaDe();
				acercaDe.setVisible(true);
			}
		});
		mnAyuda.add(mntmSobreAgenciaSeguros);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(34, 11, 390, 219);
		contentPane.add(lblNewLabel);
		Icon icono = new ImageIcon(this.getClass().getResource("logo.jpg"));
		lblNewLabel.setIcon(icono);
	}

	/**
	 * Comprueba si la agencia est� vac�a. Si lo est�, nos muestra un
	 * mensajeinformativo
	 * 
	 * @return Si la agencia est� vac�a o no
	 */
	private boolean comprobarPolizasVacias() {
		if (General.polizas.size() == 0) {
			JOptionPane.showMessageDialog(null, "No hay p�lizas en nuestra agencia", "Agencia vac�a",
					JOptionPane.INFORMATION_MESSAGE);
			return true;
		}
		return false;
	}
	
	static String getNombreAplicacion(){
		return AGENCIA_SEGUROS;
	}
}
