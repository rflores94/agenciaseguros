package agenciaSeguros.gui;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AcercaDe extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public AcercaDe() {
		setTitle("Acerca de...");
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 200, 143);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("<html>\r\n\t<head>\r\n\t\t<p>Agencia de seguros</p>\r\n\t\t<p>Roberto Carlos Flores G\u00F3mez</p>\r\n\t\t<p>Versi\u00F3n 1.0</p>\r\n\t</head>\r\n</html>");
		lblNewLabel.setBounds(10, 11, 174, 80);
		contentPanel.add(lblNewLabel);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnOk.setBounds(124, 81, 60, 23);
		contentPanel.add(btnOk);
	}
}
