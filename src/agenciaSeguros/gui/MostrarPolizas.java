package agenciaSeguros.gui;

import java.util.ListIterator;

import agenciaSeguros.funcionalidad.Poliza;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Muestra las p�lizas que existen
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class MostrarPolizas extends VentanaPadre {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ListIterator<Poliza> listIterator;
	Poliza poliza;

	/**
	 * Create the dialog.
	 */
	public MostrarPolizas() {
		setTitle("Mostrar p�lizas");
		buttonSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Poliza polizaAux;
				polizaAux = listIterator.next();
				if(polizaAux == poliza)
					poliza = listIterator.next();
				else
					poliza = polizaAux;
				mostrarPoliza();
				comprobarBotones();
			}
		});
		buttonAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Poliza polizaAux;
				polizaAux = listIterator.previous();
				if(polizaAux == poliza)
					poliza = listIterator.previous();
				else
					poliza = polizaAux;
				mostrarPoliza();
				comprobarBotones();
			}
		});
		listIterator = General.polizas.listIterator();
		ajustarVentana(190, 245);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		seguroHogar.setVisible(false);
		crear.setVisible(false);
		crearSeguro.setVisible(false);
		precio.setVisible(false);
		nombre.setEditable(false);
		dni.setEditable(false);
		direccion.setEditable(false);
		buttonAnterior.setEnabled(false);
		buttonSiguiente.setEnabled(false);
		poliza = listIterator.next();
		comprobarBotones();
		buttonAnterior.setEnabled(false);
		mostrarPoliza();
	}

	/**
	 * Muestra el cliente de una p�liza
	 */
	private void mostrarPoliza() {
		nombre.setText(poliza.getCliente().getNombre());
		dni.setText(poliza.getCliente().getDNI());
		direccion.setText(poliza.getCliente().getDireccion());
		poliza.calcularPrecio();
		lblPrecio.setText("Precio: " + poliza.getPrecio());
	}

	/**
	 * Comprueba si hay seguros antes o despu�s del que hemos mostrado para activar o desactivar los botones
	 */
	protected void comprobarBotones() {
		if (listIterator.hasPrevious())
			buttonAnterior.setEnabled(true);
		else
			buttonAnterior.setEnabled(false);
		if (!listIterator.hasNext())
			buttonSiguiente.setEnabled(false);
		else
			buttonSiguiente.setEnabled(true);
	}
	
}
