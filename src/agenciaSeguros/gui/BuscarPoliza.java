package agenciaSeguros.gui;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Busca las p�lizas y todos sus seguros
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class BuscarPoliza extends VentanaPadre {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public BuscarPoliza() {
		setTitle("Buscar p�liza");
		buttonSiguiente.addActionListener(new ActionListener() {


			public void actionPerformed(ActionEvent e) {
				mostrarSiguiente();
			}
		});
		buttonAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarAnterior();
			}
		});
		
		direccionSeguro.setEnabled(false);
		metros.setEnabled(false);
		habitaciones.setEnabled(false);
		cristales.setEnabled(false);
		incendios.setEnabled(false);
		inundaciones.setEnabled(false);
		
		matriculaCoche.setEnabled(false);
		comboBoxTipoSeguroCoche.setEnabled(false);
		
		matriculaMoto.setEnabled(false);
		comboBoxCilindrada.setEnabled(false);
		
		precio.setVisible(false);
		buttonAnterior.setEnabled(false);
		buttonSiguiente.setEnabled(false);
		comboBoxTipoSeguro.setVisible(false);
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		comboBoxTipoSeguroCoche.setVisible(false);
		crearSeguro.setVisible(false);
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarPolizaMostrar();
			}
		});
		nombre.setEditable(false);
		direccion.setEditable(false);
		crear.setText("Buscar");
		
	}
}
