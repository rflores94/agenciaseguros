package agenciaSeguros.gui;

import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Ayuda extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public Ayuda() {
		setResizable(false);
		setTitle("Ayuda");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				setVisible(false);
			}
		});
		setBounds(100, 100, 450, 474);
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel(
				"<html>\r\n<body>\r\n<ol>\r\n    <li>Ficheros</li>\r\n<ul>\r\n    <li>Nuevo: Crea una nueva agencia de seguros.</li>\r\n    <li>Abrir: Abre una agencia de seguros existente.</li>\r\n    <li>Guardar: Guarda una agencia de seguros en un archivo existente.</li>\r\n    <li>Guardar como: Guarda una agencia de seguros.</li>\r\n    <li>Salir: Se sale del programa.</li>\r\n</ul>\r\n\r\n    <li>Polizas</li>\r\n<ul>\r\n    <li>A\u00F1adir: Crea una nueva p\u00F3liza de seguros.</li>\r\n    <li>Eliminar: Elimina una p\u00F3liza de seguros existente.</li>\r\n    <li>Mostrar: Muestra todas las p\u00F3lizas de seguro existentes.</li>\r\n    <li>Buscar: Busca los seguros de una p\u00F3liza.</li>\r\n</ul>\r\n\r\n    <li>Seguros</li>\r\n<ul>\r\n    <li>A\u00F1adir: Crea un seguro dentro de una p\u00F3liza.</li>\r\n    <li>Eliminar: Elimina un seguro.</li>\r\n    <li>Editar: Edita un seguro.</li>\r\n</ul>\r\n\r\n    <li>Ayuda</li>\r\n    <ul>\r\n        <li>Ayuda: Muestra la ayuda.</li>\r\n        <li>Sobre Agencia de Seguros: Muestra los detalles del programa.</li>\r\n    </ul>\r\n</ol>\r\n</body>\r\n</html>");
		lblNewLabel.setBounds(10, 11, 414, 414);
		getContentPane().add(lblNewLabel);

	}
}
