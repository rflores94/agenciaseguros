package agenciaSeguros.gui;

import java.awt.BorderLayout;
import java.util.ListIterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import agenciaSeguros.excepciones.DNIInvlaidoException;
import agenciaSeguros.excepciones.PolizaInvalidoException;
import agenciaSeguros.funcionalidad.Cilindrada;
import agenciaSeguros.funcionalidad.Cliente;
import agenciaSeguros.funcionalidad.Conjunto;
import agenciaSeguros.funcionalidad.Poliza;
import agenciaSeguros.funcionalidad.Seguro;
import agenciaSeguros.funcionalidad.SeguroCoche;
import agenciaSeguros.funcionalidad.SeguroHogar;
import agenciaSeguros.funcionalidad.SeguroMoto;
import agenciaSeguros.funcionalidad.TipoSeguroCoche;

import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * Ventana Padre, donde se encuentran los botones, campos de texto, m�todos,
 * etc.
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class VentanaPadre extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final JPanel contentPanel = new JPanel();
	protected Poliza poliza;
	protected JTextField nombre;
	protected JTextField dni;
	protected JTextField direccion;
	protected JPanel cliente;
	protected JLabel lblNombre;
	protected JLabel lblDni;
	protected JButton crear;
	protected JTextField direccionSeguro;
	protected JTextField metros;
	protected JTextField habitaciones;
	protected JPanel seguroHogar;
	protected JTextField matriculaMoto;
	protected JLabel lblMatrcula;
	protected JLabel lblCilindrada;
	protected JTextField matriculaCoche;
	protected JPanel seguroMoto;
	protected JPanel seguroCoche;
	protected JComboBox<TipoSeguro> comboBoxTipoSeguro;
	protected JComboBox<TipoSeguroCoche> comboBoxTipoSeguroCoche;
	protected JButton crearSeguro;
	protected JCheckBox cristales;
	protected JCheckBox incendios;
	protected JCheckBox inundaciones;
	protected JComboBox<Cilindrada> comboBoxCilindrada;
	protected JTextField precio;
	protected JLabel lblPrecio;
	protected JLabel lblMatricula;
	protected JLabel lblTipoSeguro;
	protected JLabel lblNHabitaciones;
	protected JLabel lblMetrosCuadrados;
	protected JLabel lblDireccion;
	protected JLabel lblDireccin;
	protected JButton buttonAnterior;
	protected JButton buttonSiguiente;
	protected Conjunto<Seguro> seguros;
	protected ListIterator<Seguro> listIteratorSeguros;
	protected Seguro seguro;
	protected Seguro seguroAux;
	protected JLabel fechaFinal;

	/**
	 * Create the dialog.
	 */
	public VentanaPadre() {
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 540, 196);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		cliente = new JPanel();
		cliente.setBounds(4, 7, 217, 93);
		cliente.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cliente", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		contentPanel.add(cliente);
		cliente.setLayout(null);

		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(6, 19, 46, 14);
		cliente.add(lblNombre);

		lblDni = new JLabel("DNI");
		lblDni.setBounds(6, 44, 46, 14);
		cliente.add(lblDni);

		lblDireccin = new JLabel("Direcci\u00F3n");
		lblDireccin.setBounds(6, 69, 56, 14);
		cliente.add(lblDireccin);

		nombre = new JTextField();
		nombre.setBounds(62, 16, 145, 20);
		cliente.add(nombre);
		nombre.setColumns(10);

		dni = new JTextField();
		dni.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				dni.setText(dni.getText().replaceAll("[ -]", "").toUpperCase());
			}
		});
		dni.setBounds(62, 41, 145, 20);
		cliente.add(dni);
		dni.setColumns(10);

		direccion = new JTextField();
		direccion.setBounds(62, 66, 145, 20);
		cliente.add(direccion);
		direccion.setColumns(10);

		crear = new JButton("Crear");
		crear.setBounds(132, 101, 89, 23);
		contentPanel.add(crear);

		seguroHogar = new JPanel();
		seguroHogar.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Seguro Hogar",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		seguroHogar.setBounds(260, 7, 254, 118);
		contentPanel.add(seguroHogar);
		seguroHogar.setLayout(null);

		lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(6, 16, 86, 14);
		seguroHogar.add(lblDireccion);

		lblMetrosCuadrados = new JLabel("Metros cuadrados");
		lblMetrosCuadrados.setBounds(6, 41, 109, 14);
		seguroHogar.add(lblMetrosCuadrados);

		lblNHabitaciones = new JLabel("N\u00BA habitaciones");
		lblNHabitaciones.setBounds(6, 66, 109, 14);
		seguroHogar.add(lblNHabitaciones);

		direccionSeguro = new JTextField();
		direccionSeguro.setBounds(151, 16, 86, 20);
		seguroHogar.add(direccionSeguro);
		direccionSeguro.setColumns(10);

		metros = new JTextField();
		metros.setBounds(151, 41, 86, 20);
		seguroHogar.add(metros);
		metros.setColumns(10);

		habitaciones = new JTextField();
		habitaciones.setBounds(151, 66, 86, 20);
		seguroHogar.add(habitaciones);
		habitaciones.setColumns(10);

		cristales = new JCheckBox("Cristales");
		cristales.setBounds(6, 88, 67, 23);
		seguroHogar.add(cristales);

		incendios = new JCheckBox("Incendios");
		incendios.setBounds(73, 88, 76, 23);
		seguroHogar.add(incendios);

		inundaciones = new JCheckBox("Inundaciones");
		inundaciones.setBounds(151, 88, 97, 23);
		seguroHogar.add(inundaciones);

		seguroMoto = new JPanel();
		seguroMoto.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Seguro Moto",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		seguroMoto.setBounds(260, 7, 243, 68);
		contentPanel.add(seguroMoto);
		seguroMoto.setLayout(null);

		matriculaMoto = new JTextField();
		matriculaMoto.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				matriculaMoto.setText(matriculaMoto.getText().toUpperCase());
			}
		});
		matriculaMoto.setBounds(151, 16, 86, 20);
		seguroMoto.add(matriculaMoto);
		matriculaMoto.setColumns(10);

		lblMatrcula = new JLabel("Matr\u00EDcula");
		lblMatrcula.setBounds(6, 16, 86, 14);
		seguroMoto.add(lblMatrcula);

		lblCilindrada = new JLabel("Cilindrada");
		lblCilindrada.setBounds(6, 41, 86, 14);
		seguroMoto.add(lblCilindrada);

		comboBoxCilindrada = new JComboBox<Cilindrada>();
		comboBoxCilindrada.setBounds(151, 41, 86, 20);
		seguroMoto.add(comboBoxCilindrada);
		comboBoxCilindrada.setModel(new DefaultComboBoxModel<Cilindrada>(Cilindrada.values()));

		seguroCoche = new JPanel();
		seguroCoche.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Seguro Coche",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		seguroCoche.setBounds(260, 7, 243, 68);
		seguroCoche.setLayout(null);
		contentPanel.add(seguroCoche);

		matriculaCoche = new JTextField();
		matriculaCoche.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				matriculaCoche.setText(matriculaCoche.getText().toUpperCase());
			}
		});
		matriculaCoche.setBounds(117, 16, 120, 20);
		seguroCoche.add(matriculaCoche);
		matriculaCoche.setColumns(10);

		lblMatricula = new JLabel("Matricula");
		lblMatricula.setBounds(6, 16, 86, 14);
		seguroCoche.add(lblMatricula);

		lblTipoSeguro = new JLabel("Tipo Seguro");
		lblTipoSeguro.setBounds(10, 41, 82, 14);
		seguroCoche.add(lblTipoSeguro);

		comboBoxTipoSeguroCoche = new JComboBox<TipoSeguroCoche>();
		comboBoxTipoSeguroCoche.setBounds(117, 41, 120, 20);
		seguroCoche.add(comboBoxTipoSeguroCoche);
		comboBoxTipoSeguroCoche.setModel(new DefaultComboBoxModel<TipoSeguroCoche>(TipoSeguroCoche.values()));

		crearSeguro = new JButton("Crear");
		crearSeguro.setBounds(425, 124, 89, 23);
		contentPanel.add(crearSeguro);

		comboBoxTipoSeguro = new JComboBox<TipoSeguro>();
		comboBoxTipoSeguro.setModel(new DefaultComboBoxModel<TipoSeguro>(TipoSeguro.values()));
		comboBoxTipoSeguro.setBounds(270, 125, 89, 20);
		contentPanel.add(comboBoxTipoSeguro);

		lblPrecio = new JLabel("Precio: ");
		lblPrecio.setBounds(4, 105, 89, 14);
		contentPanel.add(lblPrecio);

		precio = new JTextField();
		precio.setBounds(441, 125, 73, 20);
		contentPanel.add(precio);
		precio.setColumns(10);

		buttonAnterior = new JButton("<");
		buttonAnterior.setBounds(4, 130, 60, 23);
		contentPanel.add(buttonAnterior);

		buttonSiguiente = new JButton(">");
		buttonSiguiente.setBounds(161, 130, 60, 23);
		contentPanel.add(buttonSiguiente);

		fechaFinal = new JLabel("Valido hasta: ");
		fechaFinal.setBounds(260, 128, 155, 14);
		contentPanel.add(fechaFinal);
		fechaFinal.setVisible(false);
	}

	/**
	 * Ajusta las medidas de la ventana
	 * 
	 * @param alto
	 *            Alto de la ventana
	 * @param ancho
	 *            Ancho de la ventana
	 */
	protected void ajustarVentana(int alto, int ancho) {
		setBounds(100, 100, ancho, alto);
	}

	/**
	 * Muestra el siguiente seguro de nuestra p�liza
	 */
	protected void mostrarSiguiente() {
		seguroAux = listIteratorSeguros.next();
		if (seguro == seguroAux)
			seguro = listIteratorSeguros.next();
		else
			seguro = seguroAux;
		try {
			mostrar();
		} catch (PolizaInvalidoException | DNIInvlaidoException e1) {
			JOptionPane.showMessageDialog(contentPanel, e1.getMessage(), "Error al buscar", JOptionPane.ERROR_MESSAGE);
		}
		comprobarBotones();
	}

	/**
	 * Muestra el anterior seguro de nuestra p�liza
	 */
	protected void mostrarAnterior() {
		seguroAux = listIteratorSeguros.previous();
		if (seguro == seguroAux)
			seguro = listIteratorSeguros.previous();
		else
			seguro = seguroAux;
		try {
			mostrar();
		} catch (PolizaInvalidoException | DNIInvlaidoException e) {
			JOptionPane.showMessageDialog(contentPanel, e.getMessage(), "Error al buscar", JOptionPane.ERROR_MESSAGE);
		}
		comprobarBotones();
	}

	/**
	 * Busca la p�liza de un cliente y crea el iterator
	 */
	protected void buscarPolizaMostrar() {
		try {
			poliza = General.polizas.getElemento(General.polizas.indexOf(new Poliza(new Cliente(dni.getText()))));
			nombre.setText(poliza.getCliente().getNombre());
			direccion.setText(poliza.getCliente().getDireccion());
			poliza.calcularPrecio();
			seguros = poliza.getSeguros();
			crearSeguro.setEnabled(false);
			if(seguros.size() == 0){
				buttonAnterior.setEnabled(false);
				buttonSiguiente.setEnabled(false);
				reiniciarValoresSeguro();
				seguroHogar.setVisible(false);
				seguroCoche.setVisible(false);
				seguroMoto.setVisible(false);
				fechaFinal.setVisible(false);
				precio.setVisible(false);
			}
			else {
				crearSeguro.setEnabled(true);
				lblPrecio.setText("Precio: " + poliza.getPrecio());
				fechaFinal.setVisible(true);
				precio.setVisible(true);
				precio.setEditable(false);
				listIteratorSeguros = seguros.listIterator();
				seguro = (Seguro) listIteratorSeguros.next();
				comprobarBotones();
				buttonAnterior.setEnabled(false);
				mostrar();
			}
		} catch (PolizaInvalidoException | DNIInvlaidoException e1) {
			JOptionPane.showMessageDialog(contentPanel, e1.getMessage(), "Error al buscar", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Muestra una p�liza seg�n su clase
	 * 
	 * @throws PolizaInvalidoException
	 *             Si la p�liza no es v�lida
	 * @throws DNIInvlaidoException
	 *             Si el DNI del cliente no es v�lido
	 */
	protected void mostrar() throws PolizaInvalidoException, DNIInvlaidoException {
		if (seguro.getClass() == SeguroHogar.class) {
			mostrarSeguroHogar();
		} else if (seguro.getClass() == SeguroCoche.class) {
			mostrarSeguroCoche();
		} else if (seguro.getClass() == SeguroMoto.class) {
			mostrarSeguroMoto();
		}
	}

	/**
	 * Muestra un seguro de moto
	 */
	private void mostrarSeguroMoto() {
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(true);
		SeguroMoto seguroMoto = (SeguroMoto) seguro;
		matriculaMoto.setText(seguroMoto.getMatricula());
		comboBoxCilindrada.setSelectedItem(seguroMoto.getCilindrada());
		precio.setText("Precio: " + seguroMoto.getPrecio());
		fechaFinal.setText("Valido hasta: " + seguroMoto.getFechaFinal());
	}

	/**
	 * Muestra un seguro de coche
	 */
	private void mostrarSeguroCoche() {
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(true);
		seguroMoto.setVisible(false);
		SeguroCoche seguroCoche = (SeguroCoche) seguro;
		matriculaCoche.setText(seguroCoche.getMatricula());
		comboBoxTipoSeguroCoche.setVisible(true);
		comboBoxTipoSeguroCoche.setSelectedItem(seguroCoche.getTipoSeguroCoche());
		precio.setText("Precio: " + seguroCoche.getPrecio());
		fechaFinal.setText("Valido hasta: " + seguroCoche.getFechaFinal());
	}

	/**
	 * Muestra un seguro de hogar
	 */
	private void mostrarSeguroHogar() {
		seguroHogar.setVisible(true);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		SeguroHogar seguroHogar = (SeguroHogar) seguro;
		direccionSeguro.setText(seguroHogar.getDireccion());
		metros.setText("" + seguroHogar.getMetrosCuadrados());
		habitaciones.setText("" + seguroHogar.getNumeroHacitaciones());
		cristales.setSelected(seguroHogar.isCristales());
		incendios.setSelected(seguroHogar.isIncendios());
		inundaciones.setSelected(seguroHogar.isInundaciones());
		precio.setText("Precio: " + seguroHogar.getPrecio());
		fechaFinal.setText("Valido hasta: " + seguroHogar.getFechaFinal());
	}

	/**
	 * Comprueba si hay seguros antes o despu�s del que hemos mostrado para
	 * activar o desactivar los botones
	 */
	protected void comprobarBotones() {
		if (listIteratorSeguros.hasPrevious())
			buttonAnterior.setEnabled(true);
		else
			buttonAnterior.setEnabled(false);
		if (!listIteratorSeguros.hasNext())
			buttonSiguiente.setEnabled(false);
		else
			buttonSiguiente.setEnabled(true);
	}

	/**
	 * Pone todos los valores de poliza vac�os
	 */
	protected void reiniciarValoresPoliza() {
		nombre.setText("");
		dni.setText("");
		direccion.setText("");
		precio.setVisible(false);
	}

	/**
	 * Pone todos los valores de seguro vacios
	 */
	protected void reiniciarValoresSeguro() {
		matriculaCoche.setText("");
		matriculaMoto.setText("");
		direccionSeguro.setText("");
		metros.setText("");
		habitaciones.setText("");
		cristales.setSelected(false);
		incendios.setSelected(false);
		inundaciones.setSelected(false);
	}

	/**
	 * Muestra los campos seg�n el tipo de seguro que hayamos seleccionado
	 */
	protected void seleccionarTipoSeguro() {

		switch ((TipoSeguro) comboBoxTipoSeguro.getSelectedItem()) {
		case COCHE:
			seguroCoche.setVisible(true);
			seguroHogar.setVisible(false);
			seguroMoto.setVisible(false);
			reiniciarValoresSeguro();
			break;

		case MOTO:
			seguroMoto.setVisible(true);
			seguroHogar.setVisible(false);
			seguroCoche.setVisible(false);
			reiniciarValoresSeguro();
			break;

		default:// HOGAR
			seguroHogar.setVisible(true);
			seguroCoche.setVisible(false);
			seguroMoto.setVisible(false);
			reiniciarValoresSeguro();
			break;
		}

		// if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.COCHE) {
		// seguroCoche.setVisible(true);
		// seguroHogar.setVisible(false);
		// seguroMoto.setVisible(false);
		// reiniciarValoresSeguro();
		// }
		// if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.MOTO) {
		// seguroMoto.setVisible(true);
		// seguroHogar.setVisible(false);
		// seguroCoche.setVisible(false);
		// reiniciarValoresSeguro();
		// }
		// if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.HOGAR) {
		// seguroHogar.setVisible(true);
		// seguroCoche.setVisible(false);
		// seguroMoto.setVisible(false);
		// reiniciarValoresSeguro();
		// }
	}

	/**
	 * Muestra una poliza
	 */
	protected void buscarPoliza() {
		try {
			poliza = General.polizas.getElemento(General.polizas.indexOf(new Poliza(new Cliente(dni.getText()))));
			nombre.setText(poliza.getCliente().getNombre());
			direccion.setText(poliza.getCliente().getDireccion());
			crearSeguro.setEnabled(true);
			reiniciarValoresSeguro();
		} catch (PolizaInvalidoException | DNIInvlaidoException e1) {
			JOptionPane.showMessageDialog(contentPanel, e1.getMessage(), "Error al crear", JOptionPane.ERROR_MESSAGE);
		}
	}
}
