package agenciaSeguros.gui;
import java.awt.event.ItemListener;
import java.util.ListIterator;

import javax.swing.JOptionPane;

import agenciaSeguros.excepciones.DNIInvlaidoException;
import agenciaSeguros.excepciones.PolizaInvalidoException;
import agenciaSeguros.excepciones.SeguroInvalidoException;
import agenciaSeguros.funcionalidad.Seguro;
import agenciaSeguros.funcionalidad.SeguroCoche;
import agenciaSeguros.funcionalidad.SeguroHogar;
import agenciaSeguros.funcionalidad.SeguroMoto;

import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Elimina un seguro
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class EliminarSeguro extends VentanaPadre {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public EliminarSeguro() {
		setTitle("Eliminar seguro");
		crearSeguro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resultado = JOptionPane.showConfirmDialog(contentPanel, "�Desea eliminar el seguro?");
				if(resultado == JOptionPane.YES_OPTION){
					try {
						poliza.getSeguros().eliminar(seguro);
						General.polizas.setModificado(true);
					} catch (SeguroInvalidoException e1) {
						JOptionPane.showMessageDialog(contentPanel, e1.getMessage(), "No se puede eliminar",
								JOptionPane.ERROR_MESSAGE);
					}
				}
				reiniciarValoresSeguro();
				crearSeguro.setEnabled(false);
			}
		});
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscarSeguro();
			}
		});
		comboBoxTipoSeguro.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				seleccionarTipoSeguro();
			}
		});
		crear.setText("Buscar");
		crearSeguro.setText("Eliminar");
		crearSeguro.setEnabled(false);
		metros.setEnabled(false);
		habitaciones.setEnabled(false);
		incendios.setEnabled(false);
		inundaciones.setEnabled(false);
		cristales.setEnabled(false);
		comboBoxTipoSeguroCoche.setEnabled(false);
		comboBoxCilindrada.setEnabled(false);
		crear.setLocation(103, 130);
		cliente.setVisible(false);
		buttonAnterior.setVisible(false);
		buttonSiguiente.setVisible(false);
		precio.setVisible(false);
		lblPrecio.setVisible(false);
		seguroMoto.setVisible(false);
		seguroHogar.setVisible(false);
		ajustarVentana(200, 330);
		precio.setLocation(185, 125);
		comboBoxTipoSeguro.setLocation(4, 131);
		crearSeguro.setLocation(201, 130);
		seguroCoche.setLocation(4, 7);
		seguroMoto.setLocation(4, 7);
		seguroHogar.setLocation(4, 7);
		
	}
	
	private void buscarSeguroHogar() throws PolizaInvalidoException, DNIInvlaidoException{
		for (int i = 0; i < General.polizas.size(); i++) {
			ListIterator<Seguro> itSeguro = General.polizas.getElemento(i).getSeguros().listIterator();
			while (itSeguro.hasNext()) {
				Seguro seguro = (Seguro) itSeguro.next();
				if (seguro.getClass() == SeguroHogar.class) {
					SeguroHogar sH = (SeguroHogar) seguro;
					if (sH.getDireccion().equals(direccionSeguro.getText())){
						poliza = General.polizas.getElemento(i);
						this.seguro = seguro;
						mostrar();
						return;
					}
					
				}
			}
		}
		JOptionPane.showMessageDialog(contentPanel, "No se ha encontrado el seguro", "Error al buscar", JOptionPane.WARNING_MESSAGE);
		reiniciarValoresSeguro();
	}
	
	private void buscarSeguroCoche() throws PolizaInvalidoException, DNIInvlaidoException{
		for (int i = 0; i < General.polizas.size(); i++) {
			ListIterator<Seguro> itSeguro = General.polizas.getElemento(i).getSeguros().listIterator();
			while (itSeguro.hasNext()) {
				Seguro seguro = (Seguro) itSeguro.next();
				if (seguro.getClass() == SeguroCoche.class) {
					SeguroCoche sC = (SeguroCoche) seguro;
					if (sC.getMatricula().equals(matriculaCoche.getText())){
						poliza = General.polizas.getElemento(i);
						this.seguro = seguro;
						mostrar();
						return;
					}

				}
			}
		}
		JOptionPane.showMessageDialog(contentPanel, "No se ha encontrado el seguro", "Error al buscar", JOptionPane.WARNING_MESSAGE);
		reiniciarValoresSeguro();
	}
	
	private void buscarSeguroMoto() throws PolizaInvalidoException, DNIInvlaidoException{
		for (int i = 0; i < General.polizas.size(); i++) {
			ListIterator<Seguro> itSeguro = General.polizas.getElemento(i).getSeguros().listIterator();
			while (itSeguro.hasNext()) {
				Seguro seguro = (Seguro) itSeguro.next();
				if (seguro.getClass() == SeguroMoto.class) {
					SeguroMoto sM = (SeguroMoto) seguro;
					if (sM.getMatricula().equals(matriculaMoto.getText())){
						poliza = General.polizas.getElemento(i);
						this.seguro = seguro;
						mostrar();
						return;
					}
					
				}
			}
		}
		JOptionPane.showMessageDialog(contentPanel, "No se ha encontrado el seguro", "Error al buscar", JOptionPane.WARNING_MESSAGE);
		reiniciarValoresSeguro();
	}

	/**
	 * Busca un seguro seg�n su tipo
	 */
	private void buscarSeguro() {
		try {
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.COCHE)
				buscarSeguroCoche();
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.MOTO)
			buscarSeguroMoto();
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.HOGAR)
			buscarSeguroHogar();
		crearSeguro.setEnabled(true);
		} catch (PolizaInvalidoException | DNIInvlaidoException e) {
			JOptionPane.showMessageDialog(contentPanel, "No se ha encontrado el seguro.", "Seguro no encontrado",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
