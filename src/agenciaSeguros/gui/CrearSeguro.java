package agenciaSeguros.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import agenciaSeguros.excepciones.MatriculaNoValidaException;
import agenciaSeguros.excepciones.PolizaInvalidoException;
import agenciaSeguros.excepciones.SeguroInvalidoException;
import agenciaSeguros.funcionalidad.Cilindrada;
import agenciaSeguros.funcionalidad.Seguro;
import agenciaSeguros.funcionalidad.SeguroCoche;
import agenciaSeguros.funcionalidad.SeguroHogar;
import agenciaSeguros.funcionalidad.SeguroMoto;
import agenciaSeguros.funcionalidad.TipoSeguroCoche;

import java.awt.event.ItemListener;
import java.util.ListIterator;
import java.awt.event.ItemEvent;

public class CrearSeguro extends VentanaPadre {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public CrearSeguro() {
		setTitle("Crear seguro");
		crearSeguro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comprobarAnnadirSeguro();
			}
		});
		precio.setVisible(false);
		buttonAnterior.setVisible(false);
		buttonSiguiente.setVisible(false);
		lblPrecio.setVisible(false);
		seguroHogar.setVisible(false);
		seguroMoto.setVisible(false);
		crearSeguro.setEnabled(false);
		comboBoxTipoSeguro.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				seleccionarTipoSeguro();
			}
		});
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarPoliza();
			}
		});
		nombre.setEditable(false);
		direccion.setEditable(false);
		crear.setText("Buscar");

	}

	/**
	 * A�ade un seguro de Hogar
	 */
	protected void annadirSeguroHogar() {
		if (comprobarDireccion(direccionSeguro.getText()))
			JOptionPane.showMessageDialog(contentPanel, "Ya existe un seguro de hogar con esa direcci�n.", "Error al crear",
					JOptionPane.ERROR_MESSAGE);
		else
			try {
				General.polizas.getElemento(General.polizas.indexOf(poliza)).getSeguros()
						.annadir(new SeguroHogar(direccionSeguro.getText(), Integer.parseInt(habitaciones.getText()),
								Integer.parseInt(metros.getText()), cristales.isSelected(), incendios.isSelected(),
								inundaciones.isSelected()));
				JOptionPane.showMessageDialog(contentPanel, "Se ha a�adido un seguro a la p�liza.", "Seguro a�adido",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (NumberFormatException | SeguroInvalidoException | PolizaInvalidoException e) {
				JOptionPane.showMessageDialog(contentPanel, e.getMessage(), "Error al crear",
						JOptionPane.ERROR_MESSAGE);
			}

	}

	/**
	 * A�ade un seguro de Moto
	 */
	protected void annadirSeguroMoto() {
		if (comprobarMatricula(matriculaMoto.getText()))
			JOptionPane.showMessageDialog(contentPanel, "Ya existe un veh�culo con esa matr�cula.", "Error al crear",
					JOptionPane.ERROR_MESSAGE);
		else
			try {
				General.polizas.getElemento(General.polizas.indexOf(poliza)).getSeguros().annadir(
						new SeguroMoto(matriculaMoto.getText(), (Cilindrada) comboBoxCilindrada.getSelectedItem()));
				JOptionPane.showMessageDialog(contentPanel, "Se ha a�adido un seguro a la p�liza.", "Seguro a�adido",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (SeguroInvalidoException | PolizaInvalidoException | MatriculaNoValidaException e) {
				JOptionPane.showMessageDialog(contentPanel, e.getMessage(), "Error al crear",
						JOptionPane.ERROR_MESSAGE);
			}

	}

	/**
	 * A�ade un seguro de Coche
	 */
	protected void annadirSeguroCoche() {
		if (comprobarMatricula(matriculaCoche.getText()))
			JOptionPane.showMessageDialog(contentPanel, "Ya existe un veh�culo con esa matr�cula.", "Error al crear",
					JOptionPane.ERROR_MESSAGE);
		else
			try {
				General.polizas.getElemento(General.polizas.indexOf(poliza)).getSeguros().annadir(new SeguroCoche(
						matriculaCoche.getText(), (TipoSeguroCoche) comboBoxTipoSeguroCoche.getSelectedItem()));
				JOptionPane.showMessageDialog(contentPanel, "Se ha a�adido un seguro a la p�liza.", "Seguro a�adido", JOptionPane.INFORMATION_MESSAGE);
			} catch (SeguroInvalidoException | PolizaInvalidoException | MatriculaNoValidaException e) {
				JOptionPane.showMessageDialog(contentPanel, e.getMessage(), "Error al crear",
						JOptionPane.ERROR_MESSAGE);
			}

	}

	/**
	 * Comprueba si existe a�g�n otro veh�culo con la matr�cula que le pasamos
	 * @param matricula Matricula que vamos a comprobar si existe
	 * @return true en caso de que exista, false en cualquier otro caso
	 */
	private boolean comprobarMatricula(String matricula) {		
		for (int i = 0; i < General.polizas.size(); i++) {
			ListIterator<Seguro> itSeguro = General.polizas.getElemento(i).getSeguros().listIterator();
			while (itSeguro.hasNext()) {
				Seguro seguro = (Seguro) itSeguro.next();
				if (seguro.getClass() == SeguroCoche.class) {
					SeguroCoche sC = (SeguroCoche) seguro;
					if (sC.getMatricula().equals(matricula))
						return true;
				}

				else if (seguro.getClass() == SeguroMoto.class) {
					SeguroMoto sM = (SeguroMoto) seguro;
					if (sM.getMatricula().equals(matricula))
						return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Comprueba si ya existe un seguro de hogar con la misma direccion
	 * @param direccion Direccion que vamos a comprobar
	 * @return true si existe, false en cualquier otro caso
	 */
	private boolean comprobarDireccion(String direccion) {
		for (int i = 0; i < General.polizas.size(); i++) {
			ListIterator<Seguro> itSeguro = General.polizas.getElemento(i).getSeguros().listIterator();
			while (itSeguro.hasNext()) {
				Seguro seguro = (Seguro) itSeguro.next();
				if (seguro.getClass() == SeguroHogar.class) {
					SeguroHogar sH = (SeguroHogar) seguro;
					if (sH.getDireccion().equals(direccion))
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * Comprueba el tipo de seguro que vamos a a�adir
	 */
	private void comprobarAnnadirSeguro() {
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.COCHE) {
			annadirSeguroCoche();
		}
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.MOTO) {
			annadirSeguroMoto();
		}
		if (comboBoxTipoSeguro.getSelectedItem() == TipoSeguro.HOGAR) {
			annadirSeguroHogar();
		}
		General.polizas.setModificado(true);
		reiniciarValoresSeguro();
	}
}
