package agenciaSeguros.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import agenciaSeguros.funcionalidad.Cilindrada;
import agenciaSeguros.funcionalidad.SeguroCoche;
import agenciaSeguros.funcionalidad.SeguroHogar;
import agenciaSeguros.funcionalidad.SeguroMoto;
import agenciaSeguros.funcionalidad.TipoSeguroCoche;

public class EditarSeguro extends VentanaPadre {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public EditarSeguro() {
		setTitle("Editar seguro");
		crearSeguro.setEnabled(false);
		crearSeguro.setText("Modificar");
		crearSeguro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int resultado = JOptionPane.showConfirmDialog(contentPanel, "�Desea modificar el seguro?",
						"Confirmaci�n", JOptionPane.YES_NO_OPTION);
				if (resultado == JOptionPane.YES_OPTION) {
					modificarCampos();
					JOptionPane.showMessageDialog(contentPanel, "Se ha modificado el seguro.", "Seguro modificado",
							JOptionPane.INFORMATION_MESSAGE);
					General.polizas.setModificado(true);
				}
			}
		});
		buttonSiguiente.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				mostrarSiguiente();
			}
		});
		buttonAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarAnterior();
			}
		});

		crearSeguro.setEnabled(false);
		direccionSeguro.setEnabled(false);
		metros.setEnabled(false);
		habitaciones.setEnabled(false);
		cristales.setEnabled(true);
		incendios.setEnabled(true);
		inundaciones.setEnabled(true);

		matriculaCoche.setEnabled(false);
		comboBoxTipoSeguroCoche.setEnabled(true);

		matriculaMoto.setEnabled(false);
		comboBoxCilindrada.setEnabled(true);
		precio.setVisible(false);
		buttonAnterior.setEnabled(false);
		buttonSiguiente.setEnabled(false);
		comboBoxTipoSeguro.setVisible(false);
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		comboBoxTipoSeguroCoche.setVisible(false);
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarPolizaMostrar();
				precio.setVisible(false);
			}
		});
		nombre.setEditable(false);
		direccion.setEditable(false);
		crear.setText("Buscar");
	}

	/**
	 * Modifica campos de los seguros
	 */
	private void modificarCampos() {
		if (seguro.getClass() == SeguroCoche.class) {
			SeguroCoche sC = (SeguroCoche) seguro;
			sC.setTipoSeguroCoche((TipoSeguroCoche) comboBoxTipoSeguroCoche.getSelectedItem());
			sC.calcularPrecio();
		} else if (seguro.getClass() == SeguroMoto.class) {
			SeguroMoto sM = (SeguroMoto) seguro;
			sM.setCilindrada((Cilindrada) comboBoxCilindrada.getSelectedItem());
			sM.calcularPrecio();
		} else {
			SeguroHogar sH = (SeguroHogar) seguro;
			sH.setCristales(cristales.isSelected());
			sH.setIncendios(incendios.isSelected());
			sH.setInundaciones(inundaciones.isSelected());
			sH.calcularPrecio();
		}
	}

}
