package agenciaSeguros.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.print.attribute.standard.JobKOctetsProcessed;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import agenciaSeguros.funcionalidad.Conjunto;
import agenciaSeguros.funcionalidad.Poliza;
import agenciaSeguros.utiles.Ficheros;

/**
 * Clase en la que almacenare el envoltorio
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public class General {
	static Conjunto<Poliza> polizas = new Conjunto<Poliza>();
	private static JFileChooser jFileChooser = new JFileChooser();
	private static File file = new File("SinTitulo.obj");
	private static FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos obj", "obj");
	static{jFileChooser.setFileFilter(filtro);}
	
	/**
	 * Guarda el archivo si ya existe, y si no existe ejecuta guardarComo.
	 */
	static int guardar(Principal frame) {
		if (file!= null && file.exists())
			try {
				Ficheros.guardarComo(polizas, file);
				polizas.setModificado(false);
				frame.setTitle(file.getName());
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(frame, "No se ha podido guardar", "Error al guardar",
						JOptionPane.ERROR_MESSAGE);
			}
		else {
			return guardarComo(frame);
		}
		actualizarTitulo(frame);
		return 0;
	}
	
	/**
	 * Muestra una ventana para selecionar el archivo que vamos a abrir.
	 */
	@SuppressWarnings("unchecked")
	static void abrir(Principal frame) {
		int respuesta = jFileChooser.showOpenDialog(frame);
		if(respuesta == JFileChooser.APPROVE_OPTION){
			try {
				file = jFileChooser.getSelectedFile();
				polizas = (Conjunto<Poliza>) Ficheros.abrir(file);
				actualizarTitulo(frame);
				polizas.setModificado(false);
			} catch (ClassNotFoundException | ClassCastException e) {
				JOptionPane.showMessageDialog(frame, "Archivo no v�lido.", "Archivo corrupto",
						JOptionPane.ERROR_MESSAGE);
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(frame, "No existe el archivo.", "Error al abrir",
						JOptionPane.ERROR_MESSAGE);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(frame, "Archivo no v�lido para lectura.", "Archivo corrupto",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	

	/**
	 * Muestra una ventana en la cual seleccionamos d�nde vamos a guardar el concesionario.
	 */
	static int guardarComo(Principal frame) {
		int respuesta = jFileChooser.showSaveDialog(frame);
		if (respuesta == JFileChooser.APPROVE_OPTION) {
			file = jFileChooser.getSelectedFile();
			file = Ficheros.annadirExtension(file);
			try {
				if(file.exists()){
					int resultado = JOptionPane.showConfirmDialog(frame, "�Desea sobreescribir el archivo?");
					if(resultado == 0){
						Ficheros.guardarComo(polizas, file);
//						frame.setTitle(file.getName());
					}
					else{
//						frame.setTitle(file.getName());
						return 0;
					}
				}
				else{
					Ficheros.guardarComo(polizas, file);
//					frame.setTitle(file.getName());
				}
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Archivo no v�lido para escritura.", "Archivo corructo",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		polizas.setModificado(false);
		actualizarTitulo(frame);
		return respuesta;
	}

	private static void actualizarTitulo(Principal frame) {
		frame.setTitle(Principal.getNombreAplicacion() + ": " + file.getName());
	}
	
	/**
	 * Se sale del programa, comprobando si tenemos la informaci�n guardada o no
	 */
	static void salir(Principal frame) {
		if(guardarSiModificado(frame) != JOptionPane.CANCEL_OPTION)
			System.exit(0);
	}
	
	/**
	 * Abre un fichero ya creado, comprobando si tenemos la informaci�n guardada o no
	 */
	static void abrirFichero(Principal frame) {
		if (guardarSiModificado(frame) != JOptionPane.CANCEL_OPTION)
			abrir(frame);
	}

	private static int guardarSiModificado(Principal frame) {
		if(polizas.isModificado()){
			int respuesta = JOptionPane.showConfirmDialog(frame, "Las p�lizas no est�n guardadas, �desea guardarlas?");
			switch (respuesta) {
			case JOptionPane.YES_OPTION:
				guardar(frame);
				return respuesta;
			default:
				return respuesta;
			}
		}
		return 10;
	}
	
	/**
	 * Crea una nueva agencia, comprobando si tenemos la informaci�n guardada o no
	 */
	static void crearNuevo(Principal frame) {
		if(guardarSiModificado(frame) != JOptionPane.CANCEL_OPTION){
			polizas= new Conjunto<Poliza>();
			file = new File("SinTitulo.obj");
			polizas.setModificado(false);
			actualizarTitulo(frame);
		}
	}
}
