package agenciaSeguros.gui;

import javax.swing.JOptionPane;
import agenciaSeguros.excepciones.DNIInvlaidoException;
import agenciaSeguros.excepciones.SeguroInvalidoException;
import agenciaSeguros.funcionalidad.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CrearPoliza extends VentanaPadre {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 */
	public CrearPoliza() {
		setTitle("Crear p�liza");
		precio.setVisible(false);
		buttonAnterior.setVisible(false);
		buttonSiguiente.setVisible(false);
		lblPrecio.setVisible(false);
		comboBoxTipoSeguro.setVisible(false);
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		comboBoxTipoSeguroCoche.setVisible(false);
		crearSeguro.setVisible(false);
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				annadirPoliza();
			}
		});
		ajustarVentana(170, 260);
	}

	/**
	 * A�ade una p�liza a nuestro ArrayList
	 */
	private void annadirPoliza() {
		try {
			General.polizas.annadir(new Poliza(new Cliente(nombre.getText(), dni.getText(), direccion.getText()), new Conjunto<Seguro>()));
			JOptionPane.showMessageDialog(contentPanel, "Se ha a�adido una p�liza.", "Poliza a�adida", JOptionPane.INFORMATION_MESSAGE);
			General.polizas.setModificado(true);
			reiniciarValoresPoliza();
		} catch (DNIInvlaidoException | SeguroInvalidoException e1) {
			JOptionPane.showMessageDialog(contentPanel,e1.getMessage(), "Error al a�adir", JOptionPane.ERROR_MESSAGE);
		}
	}

}
