package agenciaSeguros.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import agenciaSeguros.excepciones.SeguroInvalidoException;

/**
 * Elimina una p�liza y todos sus seguros
 * 
 * @author Roberto Carlos Flores G�mez
 * @version 1.0
 */
public class EliminarPoliza extends VentanaPadre {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btnEliminar;

	/**
	 * Create the dialog.
	 */
	public EliminarPoliza() {
		setTitle("Eliminar p�liza");
		ajustarVentana(190, 260);
		buttonAnterior.setVisible(false);
		buttonSiguiente.setVisible(false);
		direccionSeguro.setEnabled(false);
		metros.setEnabled(false);
		habitaciones.setEnabled(false);
		cristales.setEnabled(false);
		incendios.setEnabled(false);
		inundaciones.setEnabled(false);
		
		matriculaCoche.setEnabled(false);
		comboBoxTipoSeguroCoche.setEnabled(false);
		
		matriculaMoto.setEnabled(false);
		comboBoxCilindrada.setEnabled(false);
		
		precio.setVisible(false);
		buttonAnterior.setEnabled(false);
		buttonSiguiente.setEnabled(false);
		comboBoxTipoSeguro.setVisible(false);
		seguroHogar.setVisible(false);
		seguroCoche.setVisible(false);
		seguroMoto.setVisible(false);
		comboBoxTipoSeguroCoche.setVisible(false);
		crearSeguro.setVisible(false);
		crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarPolizaMostrar();
				btnEliminar.setEnabled(true);
			}
		});
		nombre.setEditable(false);
		direccion.setEditable(false);
		crear.setText("Buscar");
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminar();
			}
		});
		btnEliminar.setBounds(132, 124, 89, 23);
		contentPanel.add(btnEliminar);
		btnEliminar.setEnabled(false);
	}
	
	/**
	 * Elimina una p�liza
	 */
	private void eliminar() {
		int resultado = JOptionPane.showConfirmDialog(contentPanel, "�Desea eliminar la p�liza y todos sus seguros?");
		if(resultado == JOptionPane.YES_OPTION){
			try {
				General.polizas.eliminar(poliza);
				reiniciarValoresPoliza();
				JOptionPane.showMessageDialog(contentPanel, "La p�liza se ha eliminado", "P�liza eliminada", JOptionPane.INFORMATION_MESSAGE);
				btnEliminar.setEnabled(false);
				General.polizas.setModificado(true);
			} catch (SeguroInvalidoException e1) {
				JOptionPane.showMessageDialog(contentPanel, e1.getMessage(), "Error al eliminar", JOptionPane.ERROR_MESSAGE);
				btnEliminar.setEnabled(false);
			}
		}
	}
}
