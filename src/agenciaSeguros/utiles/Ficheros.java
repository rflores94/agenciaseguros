package agenciaSeguros.utiles;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */

public class Ficheros {

	/**
	 * Abre un fichero y lo devuelve como un Object
	 * @param fichero Fichero que vamos a abrir
	 * @return Object
	 * @throws FileNotFoundException Si el fichero no existe
	 * @throws IOException Error de entrada o salida de datos
	 * @throws ClassNotFoundException Si no existe la clase
	 */
	public static Object abrir(File fichero) throws FileNotFoundException, IOException, ClassNotFoundException {
		try (ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fichero)))) {
			return in.readObject();
		}
	}

	/**
	 * Guarda un Object que le pasamos en el fichero que queramos
	 * @param o Objeto que vamos a guardar
	 * @param fichero Fichero donde vamos a guardar el Object
	 * @throws FileNotFoundException Si el fichero no existe
	 * @throws IOException Error de entrada o salida de datos
	 */
	public static void guardarComo(Object o, File fichero) throws FileNotFoundException, IOException {
		try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fichero)))) {
			out.writeObject(o);
		}
	}
	
	/**
	 * A�ade la extension al fichero si no la lleva
	 * @param file Fichero al que le vamos a a�adir la extension
	 * @return File
	 */
	public static File annadirExtension(File file){
		if(!file.getName().endsWith(".obj"))
			file = new File(file+".obj");
		return file;
	}
}
