package agenciaSeguros.excepciones;

public class SeguroInvalidoException extends Exception {

	public SeguroInvalidoException(String string) {
		super(string);
	}

}
