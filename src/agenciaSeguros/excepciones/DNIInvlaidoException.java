package agenciaSeguros.excepciones;

public class DNIInvlaidoException extends Exception {
	public DNIInvlaidoException(String mensaje) {
		super(mensaje);
	}
}
