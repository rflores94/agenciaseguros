package agenciaSeguros.excepciones;

public class PolizaInvalidoException extends Exception {

	public PolizaInvalidoException(String string) {
		super(string);
	}

}
