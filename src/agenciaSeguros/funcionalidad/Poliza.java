package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Poliza de seguros de un cliente
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public class Poliza implements Polizable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Cliente unico de la p�liza
	 */
	private Cliente cliente;
	
	/**
	 * Conjunto de seguros de un mismo cliente
	 */
	private Conjunto<Seguro> seguros;
	
	/**
	 * Precio total de todos los seguros de la poliza
	 */
	private int precio = 0;
	
	public Poliza(Cliente cliente, Conjunto<Seguro> seguros) {
		setCliente(cliente);
		setSeguros(seguros);
		calcularPrecio();
	}
	
	public Poliza(Cliente cliente){
		setCliente(cliente);
	}
	
	public int getPrecio() {
		return precio;
	}

	private void setPrecio(int precio) {
		this.precio = precio;
	}

	public Cliente getCliente() {
		return cliente;
	}

	private void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Conjunto<Seguro> getSeguros() {
		return seguros;
	}

	private void setSeguros(Conjunto<Seguro> seguros) {
		this.seguros = seguros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poliza other = (Poliza) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Poliza [cliente=" + cliente + ", seguros=" + seguros + ", precio total= " + precio + "]";
	}

	@Override
	public void calcularPrecio() {
		int precio = 0;
		ListIterator<Seguro> it = seguros.listIterator();
		while (it.hasNext()) {
			Seguro seguro = (Seguro) it.next();
			precio += seguro.getPrecio();
		}
		setPrecio(precio);
	}
	
}
