package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

import agenciaSeguros.excepciones.MatriculaNoValidaException;
import agenciaSeguros.excepciones.PolizaInvalidoException;
import agenciaSeguros.excepciones.SeguroInvalidoException;


/**
 * Envoltorio del conjunto
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public class Conjunto<P> implements Serializable {
	private ArrayList<P> conjunto = new ArrayList<P>();
	private boolean modificado = false;
	
	/**
	 * A�ade un elemento
	 * @param elemento Elemento que vamos a a�adir
	 * @throws SeguroInvalidoException Si el seguro ya existe
	 */
	public void annadir(P elemento) throws SeguroInvalidoException{
		if(conjunto.contains(elemento))
			throw new SeguroInvalidoException("El seguro ya existe.");
		conjunto.add(elemento);
		setModificado(true);
		
	}
	
	/**
	 * Elimina un seguro
	 * @param elemento Elemento que vamos a eliminar
	 * @throws SeguroInvalidoException Si el seguro no existe
	 */
	public void eliminar(P elemento) throws SeguroInvalidoException{
		if(!conjunto.contains(elemento))
			throw new SeguroInvalidoException("El seguro no existe.");
		conjunto.remove(conjunto.indexOf(elemento));
		setModificado(true);
	}
	
	/**
	 * Devuelve el tama�o de nuestro arrayList
	 * @return Entero
	 */
	public int size(){
		return conjunto.size();
	}
	
	/**
	 * Devuelve el seguro que esta en la posicion que le pasamos
	 * @param posicion Posicion que le pasamos
	 * @return Seguro
	 */
	public P getElemento(int posicion){
		if(conjunto.isEmpty())
			return null;
		if(posicion<0 || posicion>conjunto.size()-1)
			return null;
		return conjunto.get(posicion);
	}
	
	/**
	 * Devuelve el listIterator del ArrayList
	 * @return ListIterator
	 */
	public ListIterator<P> listIterator(){
		return conjunto.listIterator();
	}
	
	public boolean isModificado() {
		return modificado;
	}

	public void setModificado(boolean modificado) {
		this.modificado = modificado;
	}
	
	/**
	 * 
	 * @param poliza Poliza de la que extraemos el �ndice
	 * @return Entero positivo
	 * @throws PolizaInvalidoException Si la p�liza no es v�lida
	 */
	public int indexOf(Poliza poliza) throws PolizaInvalidoException {
		int indice = conjunto.indexOf(poliza);
		if (indice == -1)
			throw new PolizaInvalidoException("No existe la p�liza");
		return indice;
	}
	
	/**
	 * Comprueba si contiene un seguro de vehiculol con esa matricula
	 * @param matricula Matricula que vamos a comprobar
	 * @return true si lo contiene, false si no
	 * @throws MatriculaNoValidaException Si la matricula no es v�lida
	 */
	public boolean contiene(String matricula) throws MatriculaNoValidaException{
		return conjunto.contains(new SeguroVehiculo(matricula));
	}
}
