package agenciaSeguros.funcionalidad;

import java.util.regex.Pattern;

import agenciaSeguros.excepciones.MatriculaNoValidaException;

public class SeguroVehiculo extends Seguro {

	static final private Pattern patternMatricula = Pattern.compile("^\\d{4}[ -]?[[B-Z]&&[^QEIOU]]{3}$");
	/**
	 * Coche del que guardaremos el seguro
	 */
	private String matricula;
	
	public SeguroVehiculo(String matricula) throws MatriculaNoValidaException{
		setMatricula(matricula);
	}
	
	/**
	 * Asigna la matr�cula del coche
	 * 
	 * @param matricula
	 *            Matr�cula a asignar
	 * @throws MatriculaNoValidaException
	 *             Si la matr�cula no es v�lida
	 * @see Coche#patternMatricula
	 */
	private void setMatricula(String matricula) throws MatriculaNoValidaException {
		if (!esValida(matricula))
			throw new MatriculaNoValidaException("La matr�cula no es v�lida. ");

		this.matricula = estandarizarMatricula(matricula);

	}

	public String getMatricula() {
		return this.matricula;
	}

	/**
	 * Le cambia el formato a la matr�cula del coche. u
	 * 
	 * @param matricula
	 *            Matr�cula a estandarizar. Puede tener un espacio o guion para
	 *            separar los 4 n�meros de las tres letras
	 * @return Matr�cula sin espacios ni guiones
	 */
	private String estandarizarMatricula(String matricula) {
		assert esValida(matricula);
		return matricula.replaceAll("[ -]", "");
	}
	
	/**
	 * Indica si una matr�cula es o no v�lida
	 * 
	 * @param matricula
	 * @return true si la matr�cula es v�lida. false en otro caso
	 * 
	 * @see Coche#patternMatricula
	 */
	private static boolean esValida(String matricula) {
		return patternMatricula.matcher(matricula).matches();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeguroVehiculo other = (SeguroVehiculo) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}
	
}
