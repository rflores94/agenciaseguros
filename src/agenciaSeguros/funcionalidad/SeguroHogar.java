package agenciaSeguros.funcionalidad;

import java.io.Serializable;

public class SeguroHogar extends Seguro implements Polizable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Direccion de la vivienda
	 */
	private String direccion;

	/**
	 * Numero de habitaciones
	 */
	private int numeroHacitaciones;

	/**
	 * Metros cuadrados de la vivienda
	 */
	private int metrosCuadrados;

	/**
	 * Campos del seguro
	 */
	private boolean cristales, incendios, inundaciones;

	public SeguroHogar(String direccion, int numeroHabitaciones, int metros, boolean cristales,
			boolean incendios, boolean inundaciones) {
		super();
		setDireccion(direccion);
		setNumeroHacitaciones(numeroHabitaciones);
		setMetrosCuadrados(metros);
		setCristales(cristales);
		setIncendios(incendios);
		setInundaciones(inundaciones);
		calcularPrecio();
	}

	public String getDireccion() {
		return direccion;
	}

	private void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getNumeroHacitaciones() {
		return numeroHacitaciones;
	}

	private void setNumeroHacitaciones(int numeroHacitaciones) {
		this.numeroHacitaciones = numeroHacitaciones;
	}

	public int getMetrosCuadrados() {
		return metrosCuadrados;
	}

	private void setMetrosCuadrados(int metrosCuadrados) {
		this.metrosCuadrados = metrosCuadrados;
	}

	public boolean isCristales() {
		return cristales;
	}

	public void setCristales(boolean cristales) {
		this.cristales = cristales;
	}

	public boolean isIncendios() {
		return incendios;
	}

	public void setIncendios(boolean incendios) {
		this.incendios = incendios;
	}

	public boolean isInundaciones() {
		return inundaciones;
	}

	public void setInundaciones(boolean inundaciones) {
		this.inundaciones = inundaciones;
	}

	@Override
	public String toString() {
		return super.toString() + "SeguroHogar [direccion=" + direccion + ", numeroHacitaciones=" + numeroHacitaciones
				+ ", metrosCuadrados=" + metrosCuadrados + ", cristales=" + cristales + ", incendios=" + incendios
				+ ", inundaciones=" + inundaciones + "]";
	}

	@Override
	public void calcularPrecio() {
		int precioAumento = 0;
		if(numeroHacitaciones>0 && numeroHacitaciones<=2)
			precioAumento += 50;
		else if(numeroHacitaciones>2)
			precioAumento += 80;
		
		if(metrosCuadrados>0 && metrosCuadrados<=100)
			precioAumento += 50;
		else if(metrosCuadrados>100 && metrosCuadrados<=200)
			precioAumento += 100;
		else
			precioAumento += 150;
		
		
		if(isCristales())
			precioAumento += 50;
		if(isIncendios())
			precioAumento += 100;
		if(isInundaciones())
			precioAumento += 100;
		
		setPrecio(getPrecioInicial()+precioAumento);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		SeguroHogar other = (SeguroHogar) obj;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		return true;
	}

	
}
