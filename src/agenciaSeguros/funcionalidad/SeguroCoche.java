package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.util.regex.Pattern;

import agenciaSeguros.excepciones.MatriculaNoValidaException;

/**
 * Clase SeguroCoche, hija de Seguro
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public class SeguroCoche extends SeguroVehiculo implements Polizable, Serializable {

	/**
	 * Tipo de seguro del Coche
	 */
	private TipoSeguroCoche tipoSeguroCoche;

	public SeguroCoche(String matricula, TipoSeguroCoche tipoSeguroCoche) throws MatriculaNoValidaException {
		super(matricula);
		setTipoSeguroCoche(tipoSeguroCoche);
		calcularPrecio();
	}


	public TipoSeguroCoche getTipoSeguroCoche() {
		return tipoSeguroCoche;
	}

	public void setTipoSeguroCoche(TipoSeguroCoche tipoSeguroCoche) {
		this.tipoSeguroCoche = tipoSeguroCoche;
	}

	@Override
	public void calcularPrecio() {
		switch (tipoSeguroCoche) {
		case TERCEROS:
			setPrecio(getPrecioInicial() + 100);
			break;
		case TERCEROSAMPLIABLE:
			setPrecio(getPrecioInicial() + 150);
			break;

		default:// ATODORIESGO
			setPrecio(getPrecioInicial() + 200);
			break;
		}

	}

}
