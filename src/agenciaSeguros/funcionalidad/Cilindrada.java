package agenciaSeguros.funcionalidad;
/**
 * Enumeracion Cilindrada
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public enum Cilindrada {
	/**
	 * 125
	 */
	C125,
	/**
	 * 250
	 */
	C250,
	/**
	 * 500
	 */
	C500;
	/**
	 * Almacena las clindradas
	 */
	private static final Cilindrada[] VALUES = Cilindrada.values();
	
	public Cilindrada[] getValues() {
		return VALUES;
	}
}
