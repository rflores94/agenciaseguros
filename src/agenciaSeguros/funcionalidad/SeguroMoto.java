package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.util.regex.Pattern;

import agenciaSeguros.excepciones.MatriculaNoValidaException;

public class SeguroMoto extends SeguroVehiculo implements Polizable, Serializable {
	
	/**
	 * Cilindrada de la moto
	 */
	private Cilindrada cilindrada;

	public SeguroMoto(String matricula, Cilindrada cilindrada) throws MatriculaNoValidaException {
		super(matricula);
		setCilindrada(cilindrada);
		calcularPrecio();
	}
	
	public Cilindrada getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(Cilindrada cilindrada) {
		this.cilindrada = cilindrada;
	}

	@Override
	public void calcularPrecio() {
		switch (cilindrada) {
		case C125:
			setPrecio(getPrecioInicial()+50);
			break;
		case C250:
			setPrecio(getPrecioInicial()+100);
			break;

		default://C500
			setPrecio(getPrecioInicial()+150);
			break;
		}

	}

	
}
