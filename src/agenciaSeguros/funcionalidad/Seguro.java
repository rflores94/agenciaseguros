package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Clase Seguro
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public abstract class Seguro implements Serializable {
	/**
	 * Identificador del Seguro
	 */
	private int id;
	
	/**
	 * Primer codigo del Seguro
	 */
	private static int codigo=1;
	
	/**
	 * Fecha de inicio del seguro
	 */
	private LocalDate fechaInicio;
	
	/**
	 * Fecha final del seguro
	 */
	private LocalDate fechaFinal;
	
	/**
	 * Precio del seguro
	 */
	private int precio;
	
	/**
	 * Precio inicial
	 */
	private static final int precioInicial = 100;
	
	public Seguro() {
		setId();
		setFechaInicio(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth());
		setFechaFinal(fechaInicio);
		setPrecio(precioInicial);
	}
	
	public int getPrecio() {
		return precio;
	}

	protected void setPrecio(int precio) {
		this.precio = precio;
	}

	protected int getPrecioInicial() {
		return precioInicial;
	}

	int getId() {
		return id;
	}

	protected void setId() {
		this.id = codigo++;
	}

	LocalDate getFechaInicio() {
		return fechaInicio;
	}

	protected void setFechaInicio(int anio, int mes, int dia) {
		this.fechaInicio = LocalDate.of(anio, mes, dia);
	}

	public LocalDate getFechaFinal() {
		return fechaFinal;
	}

	protected void setFechaFinal(LocalDate fechaInicio) {
		LocalDate fecha = fechaInicio.plusYears(1);
		fecha = fecha.minusDays(1);
		this.fechaFinal = fecha;
	}

	@Override
	public String toString() {
		return "Seguro [id=" + id + ", fechaInicio=" + fechaInicio + ", fechaFinal=" + fechaFinal + ", precio= " + precio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seguro other = (Seguro) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
