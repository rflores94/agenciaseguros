package agenciaSeguros.funcionalidad;

/**
 * Enumeracion TipoSeguro
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */

public enum TipoSeguroCoche {
	/**
	 * Terceros
	 */
	TERCEROS,
	/**
	 * Terceros ampliable
	 */
	TERCEROSAMPLIABLE,
	/**
	 * A todo riesgo
	 */
	ATODORIESGO;
	/**
	 * Almacena los colores posibles
	 */
	private static final TipoSeguroCoche[] VALUES = TipoSeguroCoche.values();
	
	public TipoSeguroCoche[] getValues() {
		return VALUES;
	}
}
