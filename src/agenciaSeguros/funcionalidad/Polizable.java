package agenciaSeguros.funcionalidad;
/**
 * Interfaz Polizable
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */
public interface Polizable {
	void calcularPrecio();
}
