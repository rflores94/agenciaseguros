package agenciaSeguros.funcionalidad;

import java.io.Serializable;
import java.util.regex.*;

import agenciaSeguros.excepciones.DNIInvlaidoException;

/**
 * Clase Persona
 * 
 * @author Roberto Carlos Flores Gomez
 * @version 1.0
 *
 */

public class Cliente implements Serializable {
	/**
	 * Patron del DNI valido
	 */
	private static final String PATTREN_DNI = "^(?i)\\d{8}[A-Z&&[^IOU]]$";
	/**
	 * Letras DNI
	 */
	public static final String LETRAS_DNI = "TRWAGMYFPDXBNJZSQVHLCKE";
	/**
	 * Nombre de la Persona
	 */
	private String nombre;
	/**
	 * DNI de la Persona
	 */
	private String dni;
	/**
	 * Direccion de la Persona
	 */
	private String direccion;
	
	public Cliente(String nombre, String dni, String direccion) throws DNIInvlaidoException{
		setNombre(nombre);
		setDNI(dni);
		setDireccion(direccion);
	}
	
	public Cliente(String dni) throws DNIInvlaidoException{
		setDNI(dni);
	}
	
	public String getNombre() {
		return nombre;
	}
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDNI() {
		return dni;
	}
	private void setDNI(String dni) throws DNIInvlaidoException {
		Pattern pt = Pattern.compile(PATTREN_DNI);
		Matcher m = pt.matcher(dni);
		if(m.find() && comprobarLetraDNI(dni))
			this.dni = dni.toUpperCase();
		else
			throw new DNIInvlaidoException("El DNI introducido no es valido.");
	}
	public String getDireccion() {
		return direccion;
	}
	private void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	void modificarDireccion(String nuevaDireccion){
		setDireccion(nuevaDireccion);
	}
	
	private boolean comprobarLetraDNI(String dni){
		char letra = dni.charAt(8);
		int numero = Integer.parseInt(dni.substring(0,8));
		if(letra != LETRAS_DNI.charAt(numero % 23))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return nombre + ", con DNI " + dni + " y vive en " + direccion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		return true;
	}
	
}
